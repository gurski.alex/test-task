<?php

declare(strict_types=1);

namespace App\Services\Staff;

use App\Enums\StaffFilterTypeEnum;
use App\Exceptions\StaffFilterException;
use App\Models\Staff;
use App\Services\Staff\Filter\StaffFilterService;
use Illuminate\Support\Arr;

class StaffService
{
    protected array $requestDataIndex;

    public function setRequestDataIndex(array $requestDataIndex): StaffService
    {
        $this->requestDataIndex = $requestDataIndex;
        return $this;
    }

    public function getRequestDataIndex(): array
    {
        return $this->requestDataIndex;
    }

    /**
     * @throws StaffFilterException
     */
    public function index(): array
    {
        $filter = Arr::get($this->getRequestDataIndex(), 'filter');

        $type = Arr::get($filter, 'type');

        $filterParams = Arr::only($filter, [
            'age_min',
            'age_max',
            'query',
        ]);

        $builderStaff = Staff::query();

        if (empty($type)) {
            throw new StaffFilterException('Filter type not found.', 500);
        }

        $filterEnum = StaffFilterTypeEnum::from($type);

        $filterService = new StaffFilterService($filterEnum);
        return $filterService->filter($builderStaff, $filterParams);
    }
}
