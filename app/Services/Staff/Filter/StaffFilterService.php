<?php

declare(strict_types=1);

namespace App\Services\Staff\Filter;

use App\Contracts\StaffFilterContact;
use App\Enums\StaffFilterTypeEnum;
use App\Exceptions\StaffFilterException;
use App\Services\Staff\Filter\Types\StaffFilterAgeCount;
use App\Services\Staff\Filter\Types\StaffFilterAvgAge;
use App\Services\Staff\Filter\Types\StaffFilterNameCount;
use App\Services\Staff\Filter\Types\StaffFilterNamePopular;

class StaffFilterService extends StaffFilterHelper
{
    protected StaffFilterTypeEnum $type;

    /**
     * @param StaffFilterTypeEnum $type
     */
    public function __construct(StaffFilterTypeEnum $type)
    {
        $this->type = $type;
    }


    /**
     * @throws StaffFilterException
     */
    public function getFilterInterface(): StaffFilterContact
    {
        return match ($this->type) {
            StaffFilterTypeEnum::FILTER_NAME_COUNT => new StaffFilterNameCount(),
            StaffFilterTypeEnum::FILTER_NAME_POPULAR => new StaffFilterNamePopular(),
            StaffFilterTypeEnum::FILTER_AVG_AGE => new StaffFilterAvgAge(),
            StaffFilterTypeEnum::FILTER_AGE_COUNT => new StaffFilterAgeCount(),
            default => throw new StaffFilterException('Filter type not found.', 500)
        };
    }
}
