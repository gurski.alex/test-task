<?php

declare(strict_types=1);

namespace App\Services\Staff\Filter;

use App\Contracts\StaffFilterContact;
use Illuminate\Database\Eloquent\Builder;

abstract class StaffFilterHelper
{
    abstract public function getFilterInterface(): StaffFilterContact;

    public function filter(Builder $builder, array $params): array
    {
        return $this->getfilterInterface()->filter($builder, $params);
    }

}
