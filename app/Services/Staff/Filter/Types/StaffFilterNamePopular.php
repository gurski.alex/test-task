<?php

declare(strict_types=1);

namespace App\Services\Staff\Filter\Types;

use App\Contracts\StaffFilterContact;
use App\Models\Staff;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class StaffFilterNamePopular implements StaffFilterContact
{

    public function filter(Builder $builder, array $params): array
    {
        $data = $builder->select('first_name', DB::raw('COUNT(*) as count'))
            ->groupBy('first_name')
            ->orderBy('count', 'desc')
            ->limit(10)
            ->get();

        return [
            'data' => $data
        ];
    }
}
