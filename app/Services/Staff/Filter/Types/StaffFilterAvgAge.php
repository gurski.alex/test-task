<?php

declare(strict_types=1);

namespace App\Services\Staff\Filter\Types;

use App\Contracts\StaffFilterContact;
use Illuminate\Database\Eloquent\Builder;

class StaffFilterAvgAge implements StaffFilterContact
{

    public function filter(Builder $builder, array $params): array
    {
        $data = $builder->avg('age');

        if (!empty($data))
            $data = round((float)$data);

        return [
            'data' => round($data)
        ];
    }
}
