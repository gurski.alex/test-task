<?php

declare(strict_types=1);

namespace App\Services\Staff\Filter\Types;

use App\Contracts\StaffFilterContact;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class StaffFilterAgeCount implements StaffFilterContact
{

    public function filter(Builder $builder, array $params): array
    {
        $min = Arr::get($params, 'age_min');
        $max = Arr::get($params, 'age_max');

        $data = $builder->whereBetween('age', [$min, $max])->count();

        return [
            'data' => $data
        ];
    }
}
