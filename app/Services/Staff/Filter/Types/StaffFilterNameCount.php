<?php

declare(strict_types=1);

namespace App\Services\Staff\Filter\Types;

use App\Contracts\StaffFilterContact;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class StaffFilterNameCount implements StaffFilterContact
{

    public function filter(Builder $builder, array $params): array
    {
        $query = Arr::get($params, 'query');
        $data = $builder->where('first_name', $query)->count();

        return [
            'data' => $data
        ];
    }
}
