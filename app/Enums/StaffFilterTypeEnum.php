<?php

namespace App\Enums;

enum StaffFilterTypeEnum: string
{
    /**
     * Количество пользователей с заданным именем
     */
    case FILTER_NAME_COUNT = 'filter_name_count';

    /**
     * Вывод самых популярных имен с указанием количества пользователей
     */
    case FILTER_NAME_POPULAR = 'filter_name_popular';

    /**
     * Средний возраст
     */
    case FILTER_AVG_AGE = 'filter_avg_age';

    /**
     * Количество пользователей возраст которых в заданном диапазоне
     */
    case FILTER_AGE_COUNT = 'filter_age_count';

}
