<?php

declare(strict_types=1);

namespace App\Facades;

use App\Services\Staff\StaffService;
use Illuminate\Support\Facades\Facade;

/**
 * Class StaffFacade
 * @package App\Facades
 * @mixin StaffService
 */
class StaffFacade extends Facade
{

    protected static function getFacadeAccessor(): string
    {
        return 'staff';
    }

}
