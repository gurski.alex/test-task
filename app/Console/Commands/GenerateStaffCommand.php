<?php

namespace App\Console\Commands;

use App\Models\Staff;
use Illuminate\Console\Command;

class GenerateStaffCommand extends Command
{
    protected $signature = 'generate:staff';

    protected $description = 'Command for generate list staff';

    public function handle(): void
    {
        Staff::factory()->count(5000)->create();
    }
}
