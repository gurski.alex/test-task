<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface StaffFilterContact
{
    public function filter(Builder $builder, array $params): array;
}
