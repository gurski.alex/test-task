<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\StaffFilterException;
use App\Facades\StaffFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\StaffIndexRequest;

class StaffController extends Controller
{
    /**
     * @throws StaffFilterException
     */
    public function index(StaffIndexRequest $request)
    {
        return StaffFacade::setRequestDataIndex($request->validated())->index();
    }
}
