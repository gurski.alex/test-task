<?php

namespace App\Http\Requests;

use App\Enums\StaffFilterTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StaffIndexRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'filter' => is_string($this->get('filter')) ? json_decode($this->get('filter'), true) : $this->get('filter')
        ]);
    }


    public function rules(): array
    {
        return [
            'filter' => ['required', 'array'],
            'filter.type' => ['required', Rule::enum(StaffFilterTypeEnum::class)],
            'filter.age_min' => [
                'min:1',
                'max:100',
                Rule::requiredIf($this->checkRequiredAge(StaffFilterTypeEnum::FILTER_AGE_COUNT))
            ],
            'filter.age_max' => [
                'int',
                'min:' . ($this->collect('filter')->get('age_min') ?? 1),
                'max:100',
                Rule::requiredIf($this->checkRequiredAge(StaffFilterTypeEnum::FILTER_AGE_COUNT))
            ],
            'filter.query' => [
                'string',
                Rule::requiredIf($this->checkRequiredAge(StaffFilterTypeEnum::FILTER_NAME_COUNT))
            ],
        ];
    }

    public function checkRequiredAge(StaffFilterTypeEnum $enum): bool
    {
        return $this->collect('filter')->get('type') === $enum->value;
    }

    public function attributes(): array
    {
        return [
            'filter.age_min' => 'минимальный возраст',
            'filter.age_max' => 'максимальный возраст',
            'filter.query' => 'укажите имя',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
