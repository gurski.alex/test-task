## Тестовое задание 1

Требуется реализовать сервис для получения обезличенной статистики по данным

- Количество пользователей с заданным именем
- Вывод самых популярных имен с указанием количества пользователей
- Средний возраст
- Количество пользователей возраст которых в заданном диапазоне



## Решение

1. Создана модель [Staff.php](app%2FModels%2FStaff.php) с полями first_name  и age.
2. Создан контроллер [StaffController.php](app%2FHttp%2FControllers%2FApi%2FStaffController.php) для получение данных
3. Создан request для проверки данных [StaffIndexRequest.php](app%2FHttp%2FRequests%2FStaffIndexRequest.php)
4. Создан сервис для получения данных [StaffService.php](app%2FServices%2FStaff%2FStaffService.php)
5. Создан сервис фильтр для фильтрации данных [StaffFilterService.php](app%2FServices%2FStaff%2FFilter%2FStaffFilterService.php) по Enum [StaffFilterTypeEnum.php](app%2FEnums%2FStaffFilterTypeEnum.php)


Команды для старта:

```
composer install
```

```
php -r "file_exists('.env') || copy('.env.example', '.env');"
```

Установить параметры в .env
```
DB_USERNAME|DB_PASSWORD|DB_DATABASE
APP_URL=http://127.0.0.1:8000
```

```
php artisan migrate
```

```
php artisan generate:key
```

```
php artisan generate:staff
```



```
npm install
```

Команды для проверки:

```
php artisan serve
```

```
npm run dev
```
