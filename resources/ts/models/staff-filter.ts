export interface StaffFilter {
    type: string
    query?: string | null
    age_min?: number | null
    age_max?: number | null
}
