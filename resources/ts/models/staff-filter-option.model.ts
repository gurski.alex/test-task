export interface StaffFilterOptionModel {
    title: string
    value: string
}
