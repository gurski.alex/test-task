import axios from 'axios'


const axiosIns = axios.create({
  // You can add your headers here
  // ================================
  baseURL: '/api/',
  timeout: 0,
  headers: {
    "Accept": "application/json",
    "Access-Control-Allow-Origin": "*",
    "X-Requested-With": "XMLHttpRequest",
    "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE,OPTIONS",
    "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With",
  },
})


axiosIns.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);
export default axiosIns
