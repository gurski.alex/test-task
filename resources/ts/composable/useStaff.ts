import {defineStore} from "pinia";
import axiosIns from "@axios";

export const useStaffStore = defineStore('staffStore', {
    state: () => ({
        staffData: [],
    }),
    actions: {
        fetchStaffFilter(payload: any) {
            return new Promise((resolve, reject) => {
                axiosIns.get('/staff', {
                    params: payload
                })
                    .then(response => {
                        this.staffData = response.data

                        resolve(response)
                    })
                    .catch(error => reject(error))
            })
        },
    },
    getters: {
        getStaffData(): any[] {
            return this.staffData
        },
    }
})
