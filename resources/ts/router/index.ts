
import Staff from "@/views/Staff.vue";
import {createRouter, createWebHistory} from "vue-router";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'staff',
            component: Staff,
        },
    ],
})


export default router
